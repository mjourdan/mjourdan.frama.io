module.exports = {
  buildTime: new Date(),
  baseUrl: "https://mjourdan.frama.io",
  name: "Mathieu",
  author: "mjourdan",
  copyright: "2023",
  eleventyUrl: "https://www.11ty.dev/",
  linuxfrProfileUrl: "https://linuxfr.org/users/mjourdan-2",
  en: {
    metaTitle: "Mathieu Jourdan",
    metaDescription: "doing design for free software projects",
	build: "built with",
  },
  fr: {
    metaTitle: "Mathieu Jourdan",
    metaDescription: "design d'interfaces pour des projets de logiciel libre",
	aboutIntro: "Coucou, je suis",
	aboutDescription: "Je fais du design pour le logiciel libre et les communs numériques.",
	build: "compilé avec",
  },
};
