const eleventySass = require("@11tyrocks/eleventy-plugin-sass-lightningcss");
const { EleventyI18nPlugin } = require("@11ty/eleventy");

module.exports = (eleventyConfig) => {
  eleventyConfig.addPassthroughCopy("src/img");
  eleventyConfig.addPlugin(eleventySass);
  eleventyConfig.addPlugin(EleventyI18nPlugin, {
    defaultLanguage: "en",
  });
};
